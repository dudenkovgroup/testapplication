//
//  ExchangeRateInteractor.swift
//  TestApplication
//
//  Created by igor.dudenkov on 09/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

class ExchangeRateInteractor {
    let mediator: ExchangeRateMediator
    weak var output: ExchangeRateInteractorOutput?
    
    init(mediator: ExchangeRateMediator) {
        self.mediator = mediator
    }
    
    deinit {
        unsubscribeFromExchangeRateNotification()
    }
}

// MARK: ExchangeRateInteractorInput

extension ExchangeRateInteractor: ExchangeRateInteractorInput {
    func setBaseCurrency(currency: Currency) {
        mediator.baseCurrency = currency
    }
    
    func loadExchangeRates() {
        subscribeToExchangeRateNotification()
    }
}

// MARK: Private

private extension ExchangeRateInteractor {
    func subscribeToExchangeRateNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(exchangeRateDidUpdate(notification:)),
                                               name: ExchangeRateMediator.Notifications.exchangeRateDidUpdate.name,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(exchangeRateDidUpdateWithError(notification:)),
                                               name: ExchangeRateMediator.Notifications.exchangeRateDidUpdateWithError.name,
                                               object: nil)
        
    }
    
    func unsubscribeFromExchangeRateNotification() {
        NotificationCenter.default.removeObserver(self,
                                                  name: ExchangeRateMediator.Notifications.exchangeRateDidUpdate.name,
                                                  object: nil)
        
        NotificationCenter.default.removeObserver(self,
                                                  name: ExchangeRateMediator.Notifications.exchangeRateDidUpdateWithError.name,
                                                  object: nil)
    }
}

// MARK: Handle notifications

private extension ExchangeRateInteractor {
    @objc func exchangeRateDidUpdate(notification: NSNotification) {
        guard let rates = notification.object as? ExchangeRateResult else { return }
        
        DispatchQueue.main.async {
            self.output?.didUpdate(rates: rates)
        }
    }
    
    @objc func exchangeRateDidUpdateWithError(notification: NSNotification) {
        guard let error = notification.object as? Error else { return }
        
        DispatchQueue.main.async {
            self.output?.didUpdateWith(error: error)
        }
    }
}
