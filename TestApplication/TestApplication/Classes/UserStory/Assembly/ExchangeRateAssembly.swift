//
//  ExchangeRateAssembly.swift
//  TestApplication
//
//  Created by igor.dudenkov on 09/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import UIKit

class ExchangeRateAssembly {
    static func createModule(mediator: ExchangeRateMediator) -> UIViewController {
        let view = ExchangeRateViewController.instantiateFromStoryboard()
        let interactor = ExchangeRateInteractor(mediator: mediator)
        let presenter = ExchangeRatePresenter()
        
        view.output = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        
        interactor.output = presenter
        
        return view
    }
}
