//
//  ExchangeRatePresenter.swift
//  TestApplication
//
//  Created by igor.dudenkov on 09/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

class ExchangeRatePresenter {
    weak var view: ExchangeRateViewInput?
    var interactor: ExchangeRateInteractorInput?
    
    fileprivate var currentAmount: Decimal = 100
}

// MARK: - ExchangeRateViewOutput

extension ExchangeRatePresenter: ExchangeRateViewOutput {
    func setupView() {
        let currency = Currency.defaultCurrency
        
        interactor?.setBaseCurrency(currency: currency)
        interactor?.loadExchangeRates()        
    }
    
    func didSelect(currency: CurrencyAndAmount) {
        currentAmount = currency.amount
        interactor?.setBaseCurrency(currency: currency.currency)
    }
    
    func didChange(amount: Decimal) {
        currentAmount = amount
    }
}

// MARK: - ExchangeRateInteractorOutput

extension ExchangeRatePresenter: ExchangeRateInteractorOutput {
    func didUpdate(rates: ExchangeRateResult) {
        let baseCurrency = CurrencyAndAmount(currency: rates.baseCurrency, currentAmount)
        var currencyAndAmount = makeAmountOfCurrencies(fromExchangeResult: rates)
        currencyAndAmount.insert(baseCurrency, at: 0)
        
        view?.updateView(viewData: currencyAndAmount)
    }
    
    func didUpdateWith(error: Error) {
        view?.showError()
    }
}

// MARK: - Private

private extension ExchangeRatePresenter {
    func makeAmountOfCurrencies(fromExchangeResult result: ExchangeRateResult) -> [CurrencyAndAmount]{
        return result.rates.map {
            CurrencyAndAmount(currency: $0.currency, amount: Decimal(floatLiteral: $0.rate) * currentAmount)            
        }
    }
}
