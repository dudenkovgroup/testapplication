//
//  ExchangeRateInteractorIO.swift
//  TestApplication
//
//  Created by igor.dudenkov on 09/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

protocol ExchangeRateInteractorInput {
    func setBaseCurrency(currency: Currency)
    func loadExchangeRates()
}

protocol ExchangeRateInteractorOutput: class {
    func didUpdate(rates: ExchangeRateResult)
    func didUpdateWith(error: Error)
}
