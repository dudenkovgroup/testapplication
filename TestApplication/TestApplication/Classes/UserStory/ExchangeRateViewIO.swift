//
//  ExchangeRateViewIO.swift
//  TestApplication
//
//  Created by igor.dudenkov on 09/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

typealias CurrencyAndAmount = (currency: Currency, amount: Decimal)

protocol ExchangeRateViewInput: class {
    func updateView(viewData: [CurrencyAndAmount])
    
    func showError()
}

protocol ExchangeRateViewOutput: class {
    func setupView()
    
    func didSelect(currency: CurrencyAndAmount)
    func didChange(amount: Decimal)
}
