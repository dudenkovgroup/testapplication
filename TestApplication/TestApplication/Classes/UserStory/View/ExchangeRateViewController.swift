//
//  ExchangeRateViewController.swift
//  TestApplication
//
//  Created by igor.dudenkov on 07/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import UIKit

class ExchangeRateViewController: UIViewController {
    var output: ExchangeRateViewOutput?
    
    // MARK: - IBOutlets
    @IBOutlet weak fileprivate var tableView: UITableView! {
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorStyle = .none
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.keyboardDismissMode = .onDrag
        }
    }
    
    fileprivate var currencies: [CurrencyAndAmount] = []
    fileprivate var canUpdateTable = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output?.setupView()
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

extension ExchangeRateViewController {
    static func instantiateFromStoryboard() -> ExchangeRateViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: ExchangeRateViewController.identifier) as! ExchangeRateViewController
    }
}

// MARK: - UIScrollViewDelegate
extension ExchangeRateViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        canUpdateTable = false
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        canUpdateTable = true
    }
}

// MARK: - ExchangeRateViewInput

extension ExchangeRateViewController: ExchangeRateViewInput {
    func updateView(viewData: [CurrencyAndAmount]) {
        if currencies.count > 0 {
            if canUpdateTable {
                currencies = viewData
                let paths = (1..<currencies.count - 1).map{ IndexPath(row: $0, section: 0) }
                
                tableView.beginUpdates()
                tableView.reloadRows(at: paths, with: .none)
                tableView.endUpdates()
            }
        } else {
            currencies = viewData
            tableView.reloadData()
        }
    }
    
    func showError() {
        // TODO
    }
}

// MARK: - UITableViewDelegate

extension ExchangeRateViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = (tableView.cellForRow(at: indexPath) as? ExchangeRateTableViewCell), let cellCurrency = cell.cellData else { return }
        let topIndexPath = IndexPath(row: 0, section: 0)
        
        tableView.beginUpdates()
        tableView.moveRow(at: indexPath, to: topIndexPath)
        tableView.endUpdates()
        
        output?.didSelect(currency: cellCurrency)
        
        cell.isBaseCurrency = true
        cell.output = output
    }
}

// MARK: - UITableViewDataSource

extension ExchangeRateViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ExchangeRateTableViewCell.identifier, for: indexPath) as? ExchangeRateTableViewCell else {
            return UITableViewCell()
        }
        
        cell.isBaseCurrency = indexPath.row == 0
        cell.output = cell.isBaseCurrency ? output : nil
        cell.cellData = currencies[indexPath.row]
        
        return cell
    }
}
