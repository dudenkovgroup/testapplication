//
//  ExchangeRateTableViewCell.swift
//  TestApplication
//
//  Created by igor.dudenkov on 09/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import UIKit

class ExchangeRateTableViewCell: UITableViewCell {

    @IBOutlet fileprivate weak var currencyLabel: UILabel!
    
    @IBOutlet fileprivate weak var amountTestField: UITextField! {
        didSet {
            amountTestField.textAlignment = .right
            amountTestField.isUserInteractionEnabled = false
            amountTestField.keyboardType = .decimalPad
            amountTestField.delegate = self
        }
    }
    
    weak var output: ExchangeRateViewOutput?
    
    var cellData: CurrencyAndAmount? {
        didSet {
            guard let currencyCode = cellData?.currency.currencyCode, let amount = cellData?.amount else { return }
            currencyLabel.text = currencyCode
            amountTestField.text = CurrenciesInfoService.formattedAmount(amount: amount)
        }
    }
    
    var isBaseCurrency: Bool = false {
        didSet {
            amountTestField.isUserInteractionEnabled = isBaseCurrency
            amountTestField.becomeFirstResponder()
        }
    }
    
    fileprivate lazy var invalidCharacters: CharacterSet = {
        return CharacterSet.decimalDigits.union(CharacterSet(charactersIn: CurrenciesInfoService.Localized.decimalSeparator)).inverted
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    static var identifier: String {
        return String(describing: self)
    }
}

// MARK: - UITextFieldDelegate

extension ExchangeRateTableViewCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textFieldText = textField.text {
            let amount: NSString = (textFieldText as NSString).replacingCharacters(in: range, with: string) as NSString
            output?.didChange(amount: Decimal(floatLiteral: amount.doubleValue))
        }
        
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
}
