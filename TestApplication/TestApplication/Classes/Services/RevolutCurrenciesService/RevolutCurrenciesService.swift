//
//  RevolutCurrenciesService.swift
//  TestApplication
//
//  Created by igor.dudenkov on 08/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

class RevolutCurrenciesService {
    fileprivate let apiSession = URLSession(configuration: URLSessionConfiguration.exchangeRatesConfiguration)
    fileprivate var currentDataTask: URLSessionDataTask?
    
    deinit {
        currentDataTask?.cancel()
    }
}

// MARK: - CurrenciesService

extension RevolutCurrenciesService: CurrenciesService {
    func loadExchangeRateFor(currency: Currency, handler: @escaping ResultHandler) {
        currentDataTask?.cancel()
        
        guard let endPoint = URL.exchangeRateEndPointFor(currencyCode: currency.currencyCode) else {
            handler(.failure(.unknown))
            return
        }
        
        currentDataTask = apiSession.dataTask(with: endPoint) { [weak self] data, response, error in
            defer { self?.currentDataTask = nil }
            
            switch (data, response, error) {
            case (.some(let responseData), .some, .none) where (response as? HTTPURLResponse)?.statusCode == 200:
                guard let jsonResult = (try? JSONSerialization.jsonObject(with: responseData, options: [])) as? [String: Any],
                    let result = ExchangeRateResult(jsonData: jsonResult) else {
                    handler(.failure(.unknown))
                    return;
                }
                handler(.success(result))
            case (.none, .some, .some):
                handler(.failure(.exchangeRatesLoadFaled(currency)))
            case (.none, .none, .some):
                handler(.failure(.serviceError))
            default:
                handler(.failure(.unknown))
            }
        }
        
        currentDataTask?.resume()
    }
}

// MARK: Private

private extension RevolutCurrenciesService {
    enum JsonKeys: String {
        case base
        case date
        case rates
    }
    
    enum ApiMethod: String {
        case name = "https://revolut.duckdns.org/latest"
        case query = "base="
    }
}

private extension ExchangeRateResult {
    init?(jsonData: [String: Any]) {
        guard let currencyCode = jsonData[RevolutCurrenciesService.JsonKeys.base.rawValue] as? String,
            let date = jsonData[RevolutCurrenciesService.JsonKeys.date.rawValue] as? String,
            let rates = jsonData[RevolutCurrenciesService.JsonKeys.rates.rawValue] as? [String: Double] else { return nil }
        
        self.baseCurrency = CurrenciesInfoService.currencyBy(currencyCode: currencyCode)
        self.date = date
        self.rates = rates.map {
            let currency = CurrenciesInfoService.currencyBy(currencyCode: $0.key)
            return CurrencyRate(currency: currency, rate: $0.value)
        }
    }
}

private extension URL {
    static func exchangeRateEndPointFor(currencyCode: String) -> URL? {
        guard var urlComponents = URLComponents(string: RevolutCurrenciesService.ApiMethod.name.rawValue) else { return nil }
        urlComponents.query = RevolutCurrenciesService.ApiMethod.query.rawValue + currencyCode
        return urlComponents.url
    }
}

private extension URLSessionConfiguration {
    static var exchangeRatesConfiguration: URLSessionConfiguration {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        configuration.timeoutIntervalForRequest = 5
        return configuration
    }
}
