//
//  ExchangeRateMediator.swift
//  TestApplication
//
//  Created by igor.dudenkov on 08/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

class ExchangeRateMediator {
    
    var exchangeRateUpdateTime: DispatchTimeInterval = .seconds(1) {
        didSet {
            endObserving()
            startObserving()
        }
    }
    
    var baseCurrency: Currency? {
        get {
            var result: Currency?
            queue.sync {
                result = currentCurrency
            }
            return result
        }
        
        set {
            queue.async(flags: .barrier) {
                self.currentCurrency = newValue
            }
        }
    }
    
    fileprivate let service: CurrenciesService
    fileprivate var currentCurrency: Currency?
    fileprivate let queue = DispatchQueue(label: "com.app.exchangeRate.queue.timer", attributes: .concurrent)
    fileprivate var timer: DispatchSourceTimer?
    
    init(service: CurrenciesService) {
        self.service = service
    }
    
    deinit {
        endObserving()
    }
}

// MARK: Public

extension ExchangeRateMediator {
    enum Notifications: String {
        case exchangeRateDidUpdate
        case exchangeRateDidUpdateWithError
    
        var name: NSNotification.Name {
            return NSNotification.Name(rawValue: self.rawValue)
        }
    }
}

extension ExchangeRateMediator {
    func startObserving() {
        guard timer == nil else { return }
        
        timer = DispatchSource.repeatableTimer(interval: exchangeRateUpdateTime, queue: queue) { [weak self] in
            guard let strongSelf = self, let currency = strongSelf.baseCurrency else { return }
            strongSelf.service.loadExchangeRateFor(currency: currency) { [weak self] result in
                guard let strongSelf = self else { return }
                strongSelf.queue.sync {
                    strongSelf.handleExchangeRate(result: result)
                }
            }
        }        
    }
    
    func endObserving() {
        timer?.cancel()
        timer = nil
    }
}

// MARK: Private

private extension ExchangeRateMediator {
    func handleExchangeRate(result: Result<ExchangeRateResult,CurrenciesServiceError>) {
        if (result.isSuccess) {
            guard let value = result.value, value.baseCurrency == self.baseCurrency else { return }
            NotificationCenter.default.post(name: Notifications.exchangeRateDidUpdate.name, object: result.value)
        } else {
            NotificationCenter.default.post(name: Notifications.exchangeRateDidUpdate.name, object: result.error)
        }
    }
}

private extension DispatchSource {
    class func repeatableTimer(interval: DispatchTimeInterval,
                               queue: DispatchQueue = DispatchQueue.global(),
                               handler: @escaping () -> Void) -> DispatchSourceTimer {
        let result = DispatchSource.makeTimerSource(queue: queue)
        result.schedule(deadline: .now(), repeating: interval, leeway: .milliseconds(0))
        result.setEventHandler(handler: handler)
        result.resume()
        return result
    }
}

