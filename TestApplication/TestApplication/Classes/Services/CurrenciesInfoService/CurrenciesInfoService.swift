//
//  CurrenciesInfoService.swift
//  TestApplication
//
//  Created by igor.dudenkov on 07/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

class CurrenciesInfoService {
    
    enum Localized {
        static let decimalSeparator = Locale.current.decimalSeparator ?? ","
    }
    
    static func currencyBy(currencyCode code: String) -> Currency {
        return Currency(currencyCode: code)
    }
    
    static func getListOfCurrencies() -> [Currency] {
        return Locale.commonISOCurrencyCodes.map { Currency(currencyCode: $0) }
    }
    
    static func formattedAmount(amount: Decimal) -> String {
        return amount.formattedAmount
    }
    
}

// MARK: - Pivate

private extension String {
    
    func trimmingNonDigits() -> String {
        return components(separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
    }
    
    var decimalParts: [String] {
        return components(separatedBy: CurrenciesInfoService.Localized.decimalSeparator)
    }
    
    var integerPart: String {
        return decimalParts.first?.trimmingNonDigits() ?? "0"
    }
    
    var fractionalPart: String? {
        guard decimalParts.count > 1 else { return nil }
        let fractionalPart = decimalParts[1]
        if fractionalPart.characters.count > 2 {
            return String(fractionalPart.characters.dropLast(fractionalPart.characters.count - 2))
        } else {
            return fractionalPart
        }
    }
    
    var formattedAmount: String {
        var result = integerPart
        if let fractionalPart = fractionalPart {
            result += CurrenciesInfoService.Localized.decimalSeparator + fractionalPart
        }
        return result
    }
    
}

private extension Decimal {
    
    var stringValue: String {
        return String(describing: self)
    }
    
    var positiveOrZero: Decimal {
        return self > 0 ? self : 0
    }
    
    var formattedAmount: String {
        return positiveOrZero.stringValue
            .replacingOccurrences(of: ".",
                                  with: CurrenciesInfoService.Localized.decimalSeparator)
            .formattedAmount
    }
    
}

private extension Currency {
    
    init(currencyCode: String) {
        let currencyLocale = Locale(identifier: currencyCode)
        let currencyName = (currencyLocale as NSLocale).displayName(forKey:NSLocale.Key.currencyCode, value: currencyCode)
        let countryCode = String(currencyCode.characters.prefix(2))
        let currencySymbol = (currencyLocale as NSLocale).displayName(forKey:NSLocale.Key.currencySymbol, value: currencyCode)
        
        self.init(currencyCode: currencyCode, countryCode: countryCode, currencyName: currencyName, currencySymbol: currencySymbol)
    }
    
}
