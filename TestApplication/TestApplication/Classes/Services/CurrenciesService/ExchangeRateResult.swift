//
//  ExchangeRateResult.swift
//  TestApplication
//
//  Created by igor.dudenkov on 08/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

struct ExchangeRateResult {
    let baseCurrency: Currency
    let rates: [CurrencyRate]
    let date: String
}
