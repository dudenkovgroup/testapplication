//
//  CurrenciesService.swift
//  TestApplication
//
//  Created by igor.dudenkov on 08/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

typealias ResultHandler = (Result<ExchangeRateResult,CurrenciesServiceError>) -> Void

enum CurrenciesServiceError: Swift.Error {
    case exchangeRatesLoadFaled(Currency)
    case serviceError
    case unknown
}

protocol CurrenciesService {    
    func loadExchangeRateFor(currency: Currency, handler: @escaping ResultHandler)
}
