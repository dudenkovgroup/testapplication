//
//  CurrencyRate.swift
//  TestApplication
//
//  Created by igor.dudenkov on 08/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

typealias CurrencyRate = (currency: Currency, rate: Double)
