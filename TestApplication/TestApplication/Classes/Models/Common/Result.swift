//
//  Result.swift
//  TestApplication
//
//  Created by igor.dudenkov on 08/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

enum Result<Value,Error> {
    case success(Value)
    case failure(Error)
}

extension Result {
    
    public var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
    
    public var isFailure: Bool {
        return !isSuccess
    }
    
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    public var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
    
}
