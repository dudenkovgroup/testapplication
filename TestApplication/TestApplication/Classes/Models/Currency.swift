//
//  Currency.swift
//  TestApplication
//
//  Created by igor.dudenkov on 07/10/2017.
//  Copyright © 2017 igor.dudenkov. All rights reserved.
//

import Foundation

struct Currency {
    var currencyCode:String
    var countryCode:String
    
    var currencyName:String?
    var currencySymbol:String?
    
    static var defaultCurrency: Currency {
        return CurrenciesInfoService.currencyBy(currencyCode: "EUR")
    }
}

extension Currency: Equatable {
    static func == (lhs: Currency, rhs: Currency) -> Bool {
        return lhs.currencyCode == rhs.currencyCode
    }
}
